package view;

import javax.swing.JOptionPane;

public class ViewArrayVetorDinamico {

	/* Usado para executar codigo. */
	public static void main(String[] args) {
		
		/*                   0   1   2   3   posicoes*/
		double[] valores = {9.5,5.0,7.6,9.6};
		
		String[] valores2 = new String[4];
		String[] valores3 = {"Gustavo","Java","90","gustavopaco@gmail.com"};
		int[] inteiros = new int[10];
		float[] floats = new float[10];
		
		valores2[0] = "Gustavo";
		valores2[1] = "Java";
		valores2[2] = "90";
		valores2[3] = "gustavopaco@gmail.com";
		
		for(int i = 0; i < valores3.length ; i++) {
			System.out.println("Valor na posicao" + i + " eh: " + valores3[i]);
		}
		
		
		/* Array pode ser de todos os tipos de dados e objetos tambem*/
		
		/* Array sempre deve ter a quantidade de posicoes definidas*/

		double resultado = 0;
		
		int tamanhoArray = Integer.parseInt(JOptionPane.showInputDialog("Informe a quantidade de notas."));
		double[] notas = new double[tamanhoArray];
		
		
		for(int i = 0 ; i < notas.length ; i++) {
			double nota = Double.parseDouble(JOptionPane.showInputDialog("Informe a nota " + (i+1)));
			notas[i] = nota;
			resultado += notas[i];
			System.out.println("Nota " + (i+1) + " = " + notas[i]);
		}
		
		System.out.println("Resultado: " + resultado);
	}
}
